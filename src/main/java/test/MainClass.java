package test;

public class MainClass {

	public static void main(String[] args) {
		Runnable thread = ()->{
			System.out.println("thread");
		};
		
		Thread newThread= new Thread(()->{
			System.out.println("new thread");
		});
		newThread.start();
		thread.run();
		System.out.println("End");
		System.out.println("Test");
	}

}
